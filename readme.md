Install npm:
```
apt-get install npm
```

Install is-online and twitter node packages.
```
npm install is-online
npm install twitter
```

Create a twitter app following the instruction there:
http://techknights.org/workshops/nodejs-twitterbot/ .

Stop before the `Hello World!` section.

Edit public_config.json: enter your credentials. 
Move public_config.json
```
mv public_config.json private_config.json
```

Install cron on volumio : 
```
apt-get install cron
```

Add an entry on your crontab (see here: https://www.raspberrypi.org/documentation/linux/usage/cron.md):
```
crontab -e
```
In our case, the entry is:
```
@reboot /usr/local/bin/node /home/volumio/tweet_ip.js > /home/volumio/response
```
where /usr/local/bin/node is the result of `which node`.


The file `response` may be used to debug the script if there is an issue with
your twitter credentials.

Enjoy!
